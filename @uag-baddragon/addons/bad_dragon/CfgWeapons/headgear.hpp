class bd_BaseCap_logoOnBlack: H_Cap_usblack
{
	displayname = "Bad Dragon Baseball Cap (Logo)";
	hiddenSelectionsTextures[] = {"bad_dragon\data\textures\bad_dragon_basecap_logoOnBlack.paa"};
}

class bd_BaseCap_whiteOnBlack: H_Cap_usblack
{
	displayname = "Bad Dragon Baseball Cap (White)";
	hiddenSelectionsTextures[] = {"bad_dragon\data\textures\bad_dragon_basecap_whiteOnBlack.paa"};
}

