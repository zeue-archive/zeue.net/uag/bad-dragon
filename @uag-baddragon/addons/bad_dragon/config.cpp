class cfgPatches
{
	class bad_dragon
	{
		units[] = {};
		weapons[] = {};
		requiredVersion = 1.72;
		requiredAddons[] = {};
		author[] = {"UAG Modding Team", "Cody"};
		authorUrl = "";
	}
}

class CfgWeapons
{
	#include "CfgWeapons/classes.hpp"
	#include "CfgWeapons/headgear.hpp"
}

